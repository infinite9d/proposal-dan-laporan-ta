<?php
function curl($url, $parameter) {
	$json = json_encode($parameter);
	$headers = array();
	$headers[] = 'Content-Type: application/json';
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
	curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
	
	//Send the request
	$response = curl_exec($ch);
	curl_close($ch);
	
	$result = json_decode($response, TRUE);
	return $result;
}

function curl_file_upload($url, $parameter) {
	$headers = array("Content-Type: multipart/form-data");
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1); 
	curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_POSTFIELDS, $parameter);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	
	//Send the request
	$response = curl_exec($ch);
	curl_close($ch);
	
	$result = json_decode($response, TRUE);
	return $result;
}