<?= $this->extend('layouts/layouts') ?>
 
<?= $this->section('title') ?>Home<?= $this->endSection() ?>

<?= $this->section('head') ?>
	<link rel="stylesheet" href="<?= base_url('vendor/pagination/pagination.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="masonry-loader masonry-loader-showing">
		<div class="row products product-thumb-info-list" data-plugin-masonry data-plugin-options="{'layoutMode': 'fitRows'}">
			
		</div>
		<div id="pagination-ebook" class="d-flex flex-row-reverse mt-5"></div>

		<div>
			<?php foreach ($books as $book) : ?>
				<div class="modal fade" id="modalEbook<?= str_replace(" ", "", $book['name']) ?>" tabindex="-1" role="dialog" aria-labelledby="modalEbook<?= str_replace(" ", "", $book['name']) ?>Label" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<div class="row">
									<div class="col-md-6">
										<img alt="" class="img-fluid" src="<?= $book['image'] ?>">
									</div>
									<div class="col-md-6">
										<h5><?= $book['name'] ?></h5>
										<ul class="list-unstyled">
											<li>Program Studi : <?= $book['faculty_name'] ?></li>
											<li>Penulis : <?= $book['author'] ?></li>
											<li>Dekripsi : <?= $book['description'] ?></li>
										</ul>
										<?php if (isset($_SESSION['user']) && $_SESSION['user']['status'] == 1) : ?>
											<div class="form-group">
												<label>Jumlah Hari Pinjam</label>
												<select class="form-control" id="cbDays<?= $book['code'] ?>">
													<option value="1" selected>1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
													<option value="6">6</option>
													<option value="7">7</option>
												</select>
												<sub>*Batas peminjaman adalah 7 hari, jika ingin meminjam lebih dari itu harap meminjam lagi.</sub>
											</div>
											<div class="d-flex flex-row-reverse">
												<button type="button" class="btn btn-primary btn-request-ebook" data-code="<?= $book['code'] ?>">Request Pinjam</button>
											</div>
										<?php endif ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		</div>
	</div>
<?= $this->endSection() ?>


<?= $this->section('script') ?>
	<script>
		$( ".btn-request-ebook" ).click(function(e) {
			$.ajax({
				url: "<?= site_url('student/book/request') ?>",
				type: "POST",
				data: {
					ebook: $(this).data('code'),
					days: $('#cbDays'+$(this).data('code')).find(':selected').val()
				},
				success: function(res) {
					res = JSON.parse(res)

					if (res.success) {
						toastr.success('Pengajuan peminjaman berhasil')
					}
					else {
						if (res.message) toastr.error(res.message)
						else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")
					}
				},
				error: function (res) {
					if (res.responseJSON.message) toastr.error(res.responseJSON.message)
					else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")
				}
			});/**/
		})
	</script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.5/pagination.min.js"></script>
	<script>
		var ebooks = [];
		<?php foreach ($books as $book) : ?>
			ebooks.push({
				'name' : '<?= $book['name'] ?>',
				'faculty_name' : '<?= $book['faculty_name'] ?>',
				'image' : '<?= $book['image'] ?>'
			})
		<?php endforeach ?>
		console.log(ebooks)

		var container = $('#pagination-ebook');
		container.pagination({
			dataSource: ebooks,
			pageSize: 5,
			showPageNumbers: true,
			showPrevious: true,
			showNext: true,
			showNavigator: true,
			className: 'paginationjs-theme-blue',
			callback: function(response, pagination) {
				// window.console && console.log(22, response, pagination);
				var dataHTML = '';

				$.each(response, function (index, item) {
					dataHTML += `
						<div class="col-12 col-sm-6 col-lg-3">
							<div class="product mb-0">
								<div class="product-thumb-info border-0 mb-3">
									<a href="#" data-toggle="modal" data-target="#modalEbook${(item.name).replace(/ /g, "")}">
										<div class="product-thumb-info-image">
											<img alt="" class="img-fluid" src="${item.image}">
										</div>
									</a>
								</div>
								<div class="d-flex justify-content-between">
									<div>
										<a href="#" class="d-block text-uppercase text-decoration-none text-color-default text-color-hover-primary line-height-1 text-0 mb-1">${item.faculty_name}</a>
										<h3 class="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a href="#" class="text-color-dark text-color-hover-primary">${item.name}</a></h3>
									</div>
								</div>
							</div>
						</div>
					`
				});

				container.prev().html(dataHTML);
			}
		})/**/

	</script>
<?= $this->endSection() ?>