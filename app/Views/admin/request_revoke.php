<?= $this->extend('layouts/layouts') ?>
 
<?= $this->section('title') ?>Request Revoke<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="overflow-hidden mb-1">
		<h2 class="font-weight-normal text-7 mb-0"><strong class="font-weight-extra-bold">Request Revoke</strong></h2>
	</div>
	<div class="overflow-hidden mb-4 pb-3">
		<p class="mb-0">Daftar mahasiswa yang ingin me-revoke public key-nya.</p>
	</div>
    <table class="table table-striped" id="dataTable"></table>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script>
        /*$( ".btn-approve" ).click(function(e) {
			$.ajax({
				url: "<?= site_url('admin/request/revoke') ?>",
				type: "POST",
				data: {
					code: $(this).data('code'),
                    nrp: $(this).data('nrp')
				},
				success: function(res) {
					res = JSON.parse(res)
					console.log(res)

					if (res.success) {
						toastr.success(res.message)
                        location.reload()
					}
					else {
						if (res.message) toastr.error(res.message)
						else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")
					}
				},
				error: function (res) {
					if (res.responseJSON.message) toastr.error(res.responseJSON.message)
					else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")
				}
			});
		})/**/
    </script>

    <script>
        var table;

        function generateTable() {
            table = $('#dataTable').DataTable({
                "destroy": true,
                "ajax": {
                    "url": "<?= site_url('admin/api/select-revoke') ?>",
                    "type": "GET"
                },
                "columns": [
                    {
                        title: "NRP",
                        data: "nrp"
                    },
                    {
                        title: "Nama Mahasiswa",
                        data: "name"
                    },
					{
                        title: "Public Key",
                        data: "public_key"
                    },
                    {
                        title: "Alasan",
                        data: "reason"
                    },
                    {
                        title: "Tanggal Permintaan",
                        data: "date_created",
                        render: function (data, type, row ) {
                            return moment(data).format('DD MMMM YYYY')
                        }
                    },
                    {
                        title: "Opsi",
                        render: function (data, type, row) {
                            return `
                                <button type="button" class="btn btn-primary btn-approve" data-code="${row.code}" data-nrp="${row.nrp}">Approve</button>
                            `
                        }
                    }
                ]
            })
        }

        $(document).ready(function() {
            generateTable()

            $(document).on("click", ".btn-approve", function () {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Setuju untuk me-revoke public key mahasiswa ini.",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, approve!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: "<?= site_url('admin/request/revoke') ?>",
                            type: "POST",
                            data: {
                                code: $(this).data('code'),
                                nrp: $(this).data('nrp')
                            },
                            success: function(res) {
                                res = JSON.parse(res)
                                console.log(res)

                                if (res.success) {
                                    toastr.success(res.message)
                                    generateTable()
                                }
                                else {
                                    if (res.message) toastr.error(res.message)
                                    else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")
                                }
                            },
                            error: function (res) {
                                if (res.responseJSON.message) toastr.error(res.responseJSON.message)
                                else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")
                            }
                        })
                    }
                })
            })
        })
	</script>
<?= $this->endSection() ?>