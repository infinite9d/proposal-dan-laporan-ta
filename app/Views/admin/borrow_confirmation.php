<?= $this->extend('layouts/layouts') ?>
 
<?= $this->section('title') ?>Konfirmasi Peminjaman<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="overflow-hidden mb-1">
		<h2 class="font-weight-normal text-7 mb-0"><strong class="font-weight-extra-bold">Konfirmasi Peminjaman</strong></h2>
	</div>
	<div class="overflow-hidden mb-4 pb-3">
		<p class="mb-0">Konfirmasi peminjaman ebook yang diingikan oleh mahasiswa!</p>
	</div>
    <table class="table table-striped" id="dataTable"></table>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script>
        var table;

        function generateTable() {
            table = $('#dataTable').DataTable({
                "destroy": true,
                "ajax": {
                    "url": "<?= site_url('admin/api/waiting-confirmation') ?>",
                    "type": "GET"
                },
                "columnDefs": [
                    { "className": "text-right", "targets": [3] },
                ],
                "columns": [
                    {
                        title: "NRP",
                        data: "nrp"
                    },
                    {
                        title: "Nama Mahasiswa",
                        data: "name"
                    },
                    {
                        title: "Nama Buku",
                        data: "book_name"
                    },
                    {
                        title: "Jumlah Hari",
                        data: "day_loan"
                    },
                    {
                        title: "Tanggal Permintaan",
                        data: "date_created",
                        render: function (data, type, row ) {
                            return moment(data).format('DD MMMM YYYY')
                        }
                    },
                    {
                        title: "Opsi",
                        render: function (data, type, row) {
                            return `
                                <button type="button" class="btn btn-primary btn-approve" data-code="${row.code}">Approve</button>
                                <button type="button" class="btn btn-danger btn-reject" data-code="${row.code}">Reject</button>
                            `
                        }
                    }
                ]
            })
        }

        $(document).ready(function() {
            generateTable()

            $(document).on("click", ".btn-approve", function () {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Menerima permintaan peminjaman ini.",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, approve!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: "<?= site_url('admin/request/approve') ?>",
                            type: "POST",
                            data: {
                                code: $(this).data('code')
                            },
                            success: function(res) {
                                res = JSON.parse(res)
                                console.log(res)

                                if (res.success) {
                                    toastr.success(res.message)
                                    generateTable()
                                }
                                else {
                                    if (res.message) toastr.error(res.message)
                                    else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")
                                }
                            },
                            error: function (res) {
                                if (res.responseJSON.message) toastr.error(res.responseJSON.message)
                                else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")
                            }
                        })
                    }
                })
            })

            $(document).on("click", ".btn-reject", async function () {
                const { value: text } = await Swal.fire({
                    input: 'textarea',
                    inputLabel: 'Reason',
                    inputPlaceholder: 'Type your reason here...',
                    inputAttributes: {
                        'aria-label': 'Type your reason here'
                    },
                    showCancelButton: true
                })

                if (text) {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "Alasan yang dimasukkan valid dan ingin menolak peminjaman ini!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, reject!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                url: "<?= site_url('admin/request/reject') ?>",
                                type: "POST",
                                data: {
                                    code: $(this).data('code'),
                                    reason: text
                                },
                                success: function(res) {
                                    res = JSON.parse(res)
                                    console.log(res)

                                    if (res.success) {
                                        toastr.success(res.message)
                                        generateTable()
                                    }
                                    else {
                                        if (res.message) toastr.error(res.message)
                                        else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")
                                    }
                                },
                                error: function (res) {
                                    if (res.responseJSON.message) toastr.error(res.responseJSON.message)
                                    else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")
                                }
                            });
                        }
                    })
                }
			})
        })
	</script>
<?= $this->endSection() ?>