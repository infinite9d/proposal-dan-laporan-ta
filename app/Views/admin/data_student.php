<?= $this->extend('layouts/layouts') ?>
 
<?= $this->section('title') ?>Data Mahasiswa<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="overflow-hidden mb-1">
		<h2 class="font-weight-normal text-7 mb-0"><strong class="font-weight-extra-bold">Data Mahasiswa</strong></h2>
	</div>
	<div class="overflow-hidden mb-4 pb-3">
		<p class="mb-0">Daftar mahasiswa yang melakukan pendaftaran pada website perpustakaan TA ISTTS.</p>
	</div>
    <table class="table table-striped" id="dataTable"></table>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script>
        var table;

        function generateTable() {
            table = $('#dataTable').DataTable({
                "destroy": true,
                "ajax": {
                    "url": "<?= site_url('admin/api/select-user') ?>",
                    "type": "GET"
                },
                "columnDefs": [
                    { "width": "15%", "targets": 0 },
                    { "width": "25%", "targets": 1 },
                    { "width": "60%", "targets": 2 }
                ],
                "columns": [
                    {
                        title: "NRP",
                        data: "nrp"
                    },
                    {
                        title: "Nama Mahasiswa",
                        data: "name"
                    },
                    {
                        title: "Program Studi",
                        data: "faculty_name"
                    },
                    {
                        title: "Public Key",
                        data: "public_key"
                    }
                ]
            })
        }

        $(document).ready(function() {
            generateTable()
        })
	</script>
<?= $this->endSection() ?>