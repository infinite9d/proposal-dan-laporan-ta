<?= $this->extend('layouts/layouts') ?>
 
<?= $this->section('title') ?>Tambah Ebook<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="overflow-hidden mb-1">
		<h2 class="font-weight-normal text-7 mb-0"><strong class="font-weight-extra-bold">Tambah</strong> Ebook</h2>
	</div>
	<div class="overflow-hidden mb-4 pb-3">
		<p class="mb-0">Menambah ebook tugas akhir mahasiswa pada website perpustakaan TA ISTTS.</p>
	</div>
	<form id="formBook" method="POST" enctype="multipart/form-data">
		<div class="form-row">
			<div class="form-group col-md-12">
				<label>Mahasiswa</label>
				<select class="form-control selectpicker" name="student" id="cbStudent" data-live-search="true">
					<?php foreach ($users as $user) : ?>
						<option value="<?= $user['code'] ?>" data-subtext="<?= $user['nrp'] ?>"><?= $user['name'] ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
		<div class="form-row">
			<div class="form-group col-md-12">
				<label class="required text-2">Nama Tugas Akhir</label>
				<input type="text" value="" class="form-control" name="name" id="txtName" required>
				<div class="invalid-feedback" id="addebook-invalid-name"></div>
			</div>
		</div>
		<div class="form-row">
			<div class="form-group col-md-12">
				<label>Poster Tugas Akhir</label>
				<div class="text-center">
					<img id="imgCover" src="#" alt="Cover" width="400"/>
				</div>
				<input class="d-block" type="file" id="fuImage" accept="image/*">
				<div class="invalid-feedback" id="addebook-invalid-image"></div>
			</div>
		</div>
		<div class="form-row">
			<div class="form-group col-md-12">
				<label>File Ebook Tugas Akhir (PDF)</label>
				<input class="d-block" type="file" name="file" id="fuFile" accept="application/pdf">
				<div class="invalid-feedback" id="addebook-invalid-file"></div>
			</div>
		</div>
		<div class="form-row">
			<div class="form-group col-md-12 mb-4">
				<label class="text-2">Deskripsi</label>
				<textarea class="form-control" name="description" id="txtDescription"></textarea>
			</div>
		</div>
		
		<div class="form-row">
			<div class="form-group col-md-12 mb-5 text-right">
				<input type="submit" id="btnSubmit" value="Tambah Ebook" class="btn btn-primary btn-modern pull-right" data-loading-text="Loading...">
			</div>
		</div>
	</form>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script>
		$('#fuFile').addClass('is-invalid')
		$( "#btnSubmit" ).click(function(e) {
			e.preventDefault()

			var formData = new FormData($('#formBook')[0]);
			formData.append('image', image)
			formData.append('is_active', 1)

			$(':input', '#formBook').removeClass('is-invalid is-valid')
        	$('.invalid-feedback', '#formBook').html('')

			$.ajax({
				url: "<?= site_url('admin/book/insert') ?>",
				type: "POST",
				data: formData,
				processData: false,
				contentType: false,
				cache: false,
				success: function(res) {
					res = JSON.parse(res)

					if (res.success) {
						toastr.success(res.message)

						$('input[type="text"], input[type="file"]', '#formBook').val('')
						$(':input', '#formBook').removeClass('is-invalid is-valid')
						$('.invalid-feedback', '#formBook').html('')
					}
					else {
						if (res.message) toastr.error(res.message)
						else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")

						if (res.data.error_name) {
							$('input[name="name"]', '#formBook').addClass('is-invalid')
							$('#addebook-invalid-name').html(res.data.error_name)
						}
						if (res.data.error_image) {
							$('#fuImage').addClass('is-invalid')
							$('#addebook-invalid-image').html(res.data.error_image)
						}
						if (res.data.error_file) {
							$('#fuFile').addClass('is-invalid')
							$('#addebook-invalid-file').html(res.data.error_file)
						}
					}
				},
				error: function (res) {
					if (res.responseJSON.message) toastr.error(res.responseJSON.message)
					else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")
				}
			});
		})
	</script>

	<!-- Image -->
    <script>
        var image = ""
        $("#imgCover").hide()

        // Upload Image
        $("#fuImage").change(function() {
            readImage(this);
        })

        function readImage(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $("#imgCover").attr('src', e.target.result)
                    $("#imgCover").show()
                    image = e.target.result
                };
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }
    </script>
<?= $this->endSection() ?>