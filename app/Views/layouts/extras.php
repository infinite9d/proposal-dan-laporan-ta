<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="p-4">
                    <h4>Langkah-langkah Pemakaian Website</h4>
                    <p>Berikut cara melakukan peminjaman buku:</p>
                    <ol class="list list-ordened">
                        <li>
                            <div>Pastikan anda mempunyai Public Key dan Private Key</div>
                            <sub>*Anda bisa men-generate public key dan private key setelah anda mendownload software pada website ini (Icon Download)</sub>
                        </li>
                        <li>
                            <div>Download, extract 'here' file rar, dan taruh di folder htdocs pada XAMPP anda.</div>
                            <sub>*Pastikan anda sudah <b>menginstal XAMPP</b>.</sub>
                        </li>
                        <li>
                            <div>Login pada website local dan pilih menu "Generate Key"</div>
                            <sub>
                                *Pastikan XAMPP sudah berjalan & service Apache dijalankan.
                                <br>
                                Buka halaman website <a href="http://localhost/ta-client/public" target="_blank">http://localhost/ta-client/public</a>
                                dan simpan private key baik-baik private key anda.
                            </sub>
                        </li>
                        <li>Login pada <a href="https://perpustakaan-ta-istts.com" target="_blank">https://perpustakaan-ta-istts.com</a></li>
                        <li>Dan kalian bisa meminjam buku TA yang kalian inginkan.</li>
                    </ol>
                    <div class="d-flex flex-row-reverse">
                        <a href="<?= base_url('User Manual.pdf') ?>" target="_blank" class="btn btn-primary">User Manual</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body m-3">
                <h4 class="modal-title mb-3" id="formModalLabel">Login</h4>
                <form id="login-form" class="mb-4" novalidate="novalidate">
                    <div class="form-group row align-items-center">
                        <label class="col-sm-3 text-left text-sm-right mb-0">NRP</label>
                        <div class="col-sm-9">
                            <input type="text" name="username" class="form-control" required />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 text-left text-sm-right mb-0">Password</label>
                        <div class="col-sm-9">
                            <input type="password" name="password" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btnLogin">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body m-3">
                <h4 class="modal-title mb-3" id="formModalLabel">Register</h4>
                <form id="register-form" class="mb-4" novalidate="novalidate">
                    <div class="form-group row align-items-center">
                        <label class="col-sm-3 text-left text-sm-right mb-0">Program Studi</label>
                        <div class="col-sm-9">
                            <select name="faculty" class="form-control">
                                <option value="1" selected>Informatika</option>
                                <option value="2">Sistem Informasi Bisnis</option>
                                <option value="3">Desain Komunikasi Visual</option>
                                <option value="4">Desain Produk</option>
                                <option value="5">Teknik Elektro</option>
                                <option value="6">Teknik Industri</option>
                                <option value="7">D3 Sistem Informasi</option>
                                <option value="8">S1 Informatika Professional</option>
                                <option value="9">S2 Teknologi Informasi</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label class="col-sm-3 text-left text-sm-right mb-0">NRP</label>
                        <div class="col-sm-9">
                            <input type="text" name="username" class="form-control" required />
                            <div class="invalid-feedback" id="register-invalid-username"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 text-left text-sm-right mb-0">Password</label>
                        <div class="col-sm-9">
                            <input type="password" name="password" class="form-control" />
                            <div class="invalid-feedback" id="register-invalid-password"></div>
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label class="col-sm-3 text-left text-sm-right mb-0">Nama</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" class="form-control" required />
                            <div class="invalid-feedback" id="register-invalid-name"></div>
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label class="col-sm-3 text-left text-sm-right mb-0">Email</label>
                        <div class="col-sm-9">
                            <input type="email" name="email" class="form-control" required />
                            <div class="invalid-feedback" id="register-invalid-email"></div>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btnRegister">Register</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="p-4">
                    <h4>Profile</h4>
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <span class="d-block font-weight-normal line-height-1 text-dark">NRP</span>
                            <?php if (isset($_SESSION['user'])) echo $_SESSION['user']['username']; ?>
                        </div>
                        <div class="col-md-4 mb-3">
                            <span class="d-block font-weight-normal line-height-1 text-dark">Nama</span>
                            <?php if (isset($_SESSION['user'])) echo $_SESSION['user']['name']; ?>
                        </div>
                        <div class="col-md-4 mb-3">
                            <span class="d-block font-weight-normal line-height-1 text-dark">Status</span>
                            <?php 
                                if (isset($_SESSION['user'])) {
                                    echo ($_SESSION['user']['status'] == 1) ? "Mahasiswa" : "Dosen / Admin";
                                }
                            ?>
                        </div>
                        <div class="col-md-12 mb-3">
                            <span class="d-block font-weight-normal line-height-1 text-dark">Public Key</span>
                            <?php if (isset($_SESSION['user'])) echo $_SESSION['user']['public_key']; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>