<script>
    $('#btnLogin').click(function() {
        var frm = $("#login-form")
        var formData = new FormData(frm[0])
        // formData.append('_token', "{{ csrf_token() }}");

        for (var pair of formData.entries()) {
            console.log(pair[0]+ ', ' + pair[1]); 
        }

        $.ajax({
            method: "POST",
            url: "<?= site_url('login') ?>",
            data: formData,
            processData: false,
            contentType: false,
            success : function(res) {
                res = JSON.parse(res)
                
                if (res.success) {
                    window.location.href = "<?= site_url('/') ?>"
                    // toastr.success(res.message)
                }
                else {
                    if (res.message) toastr.error(res.message)
                    else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")

                    $(':input', '#loginModal').addClass('is-invalid')
                }
            },
            error: function (res) {
                if (res.responseJSON.message) toastr.error(res.responseJSON.message)
                else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")
            }
        });
    })

    $('#btnRegister').click(function() {
        var frm = $("#register-form")
        var formData = new FormData(frm[0])
        // formData.append('_token', "{{ csrf_token() }}");

        for (var pair of formData.entries()) {
            console.log(pair[0]+ ', ' + pair[1]); 
        }
        $(':input', '#registerModal').removeClass('is-invalid is-valid')
        $('.invalid-feedback', '#registerModal').html('')

        $.ajax({
            method: "POST",
            url: "<?= site_url('register') ?>",
            data: formData,
            processData: false,
            contentType: false,
            success : function(res) {
                res = JSON.parse(res)
                
                if (res.success) {
                    toastr.success(res.message)
                    toastr.success('Cek email anda untuk melakukan verifikasi email.')
                    
                    $('#registerModal').modal('hide')
                    $('input[type="text"], input[type="password"], input[type="email"]', '#registerModal').val('')
                    $(':input', '#registerModal').removeClass('is-invalid is-valid')
                    $('.invalid-feedback', '#registerModal').html('')

                    $('#infoModal').modal('show')
                }
                else {
                    if (res.message) toastr.error(res.message)
                    else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")

                    if (res.data.error_username) {
                        $('input[name="username"]', '#registerModal').addClass('is-invalid')
                        $('#register-invalid-username').html(res.data.error_username)
                    }
                    if (res.data.error_password) {
                        $('input[name="password"]', '#registerModal').addClass('is-invalid')
                        $('#register-invalid-password').html(res.data.error_password)
                    }
                    if (res.data.error_name) {
                        $('input[name="name"]', '#registerModal').addClass('is-invalid')
                        $('#register-invalid-name').html(res.data.error_name)
                    }
                    if (res.data.error_email) {
                        $('input[name="email"]', '#registerModal').addClass('is-invalid')
                        $('#register-invalid-email').html(res.data.error_email)
                    }
                }
            },
            error: function (res) {
                if (res.responseJSON.message) toastr.error(res.responseJSON.message)
                else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")
            }
        });
    })

    $('#btnSearch').click(function() {
        var frm = $("#form-search")
        var formData = new FormData(frm[0])

        for (var pair of formData.entries()) {
            console.log(pair[0]+ ', ' + pair[1]); 
        }

        $.ajax({
            method: "POST",
            url: "<?= site_url('search') ?>",
            data: formData,
            processData: false,
            contentType: false,
            success : function(res) {
                res = JSON.parse(res)
                
                if (res.success) {
                    window.location.href = "<?= site_url('/') ?>"
                }
                else {
                    if (res.message) toastr.error(res.message)
                    else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")
                }
            },
            error: function (res) {
                if (res.responseJSON.message) toastr.error(res.responseJSON.message)
                else toastr.error("Terjadi Kesalahan Pada Sistem, Silahkan Coba Lagi")
            }
        });/**/
    })

    $('#menuAddEbookMHS').click(function() {
        Swal.fire({
            title: 'Apakah anda sudah lulus?',
            text: "Menu ini hanya untuk mahasiswa yang sudah lulus.",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, saya sudah lulus'
        }).then((result) => {
            if (result.isConfirmed) {
                location.href = "<?= site_url('student/add-ebook') ?>"
            }
        })
    })
</script>

<script>
    $('#loginModal').on('hidden.bs.modal', function (e) {
        $('input[type="text"], input[type="password"]', '#loginModal').val('')
        $(':input', '#loginModal').removeClass('is-invalid is-valid')
    })

    $('#registerModal').on('hidden.bs.modal', function (e) {
        $('input[type="text"], input[type="password"], input[type="email"]', '#registerModal').val('')
        $(':input', '#registerModal').removeClass('is-invalid is-valid')
        $('.invalid-feedback', '#registerModal').html('')
    })
</script>