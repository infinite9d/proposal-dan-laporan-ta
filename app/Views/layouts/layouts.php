<!DOCTYPE html>
<html>
   	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?= $this->renderSection('title') ?>  | E-Lib iSTTS</title>

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<link rel="shortcut icon" href="<?= base_url('img/favicon.ico') ?>" type="image/x-icon" />
		<link rel="apple-touch-icon" href="<?= base_url('img/apple-touch-icon.png') ?>">

		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<link id="googleFonts" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400&amp;display=swap" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<?= base_url('vendor/bootstrap/css/bootstrap.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('vendor/fontawesome-free/css/all.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('vendor/animate/animate.compat.css') ?>">
		<link rel="stylesheet" href="<?= base_url('vendor/simple-line-icons/css/simple-line-icons.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('vendor/owl.carousel/assets/owl.carousel.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('vendor/owl.carousel/assets/owl.theme.default.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('vendor/magnific-popup/magnific-popup.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('vendor/bootstrap-star-rating/css/star-rating.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('vendor/bootstrap-star-rating/themes/krajee-fas/theme.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('css/theme.css') ?>">
		<link rel="stylesheet" href="<?= base_url('css/theme-elements.css') ?>">
		<link rel="stylesheet" href="<?= base_url('css/theme-blog.css') ?>">
		<link rel="stylesheet" href="<?= base_url('css/theme-shop.css') ?>">
		<link id="skinCSS" rel="stylesheet" href="<?= base_url('css/skins/default.css') ?>">
		<link rel="stylesheet" href="<?= base_url('css/custom.css') ?>">
		<script src="<?= base_url('vendor/modernizr/modernizr.min.js') ?>"></script>

		<!-- New CSS -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.12.0/af-2.4.0/b-2.2.3/b-colvis-2.2.3/b-html5-2.2.3/b-print-2.2.3/cr-1.5.6/date-1.1.2/fc-4.1.0/fh-3.2.3/kt-2.7.0/r-2.3.0/rg-1.2.0/rr-1.2.8/sc-2.0.6/sb-1.3.3/sp-2.0.1/sl-1.4.0/sr-1.1.1/datatables.min.css"/>

        <!-- Head -->
        <?= $this->renderSection('head') ?>

	</head>
	<body data-plugin-page-transition>
		<div class="body">
			<!-- <div class="notice-top-bar bg-primary" data-sticky-start-at="180">
				<button class="hamburguer-btn hamburguer-btn-light notice-top-bar-close m-0 active" data-set-active="false">
					<span class="close">
						<span></span>
						<span></span>
					</span>
				</button>
				<div class="container">
					<div class="row justify-content-center py-2">
						<div class="col-9 col-md-12 text-center">
							<p class="text-color-light font-weight-semibold mb-0">Get Up to <strong>40% OFF</strong> New-Season Styles <a href="#" class="btn btn-primary-scale-2 btn-modern btn-px-2 btn-py-1 ml-2">MEN</a> <a href="#" class="btn btn-primary-scale-2 btn-modern btn-px-2 btn-py-1 ml-1 mr-2">WOMAN</a> <span class="opacity-6 text-1">* Limited time only.</span></p>
						</div>
					</div>
				</div>
			</div> -->

            <!-- Navigation -->
            <?= $this->include('layouts/navigation') ?>
			

			<div role="main" class="main shop pt-4">
				<div class="container">
                    <?= $this->renderSection('content') ?>
				</div>
			</div>

            <!-- Footer -->
            <?= $this->include('layouts/footer') ?>
			
		</div>

        <!-- Extras -->
        <?= $this->include('layouts/extras') ?>
	  
		<script src="<?= base_url('vendor/jquery/jquery.min.js') ?>"></script>
		<script src="<?= base_url('vendor/jquery.appear/jquery.appear.min.js') ?>"></script>
		<script src="<?= base_url('vendor/jquery.easing/jquery.easing.min.js') ?>"></script>
		<script src="<?= base_url('vendor/jquery.cookie/jquery.cookie.min.js') ?>"></script>
		<script src="<?= base_url('vendor/popper/umd/popper.min.js') ?>"></script>
		<script src="<?= base_url('vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
		<script src="<?= base_url('vendor/jquery.validation/jquery.validate.min.js') ?>"></script>
		<script src="<?= base_url('vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js') ?>"></script>
		<script src="<?= base_url('vendor/jquery.gmap/jquery.gmap.min.js') ?>"></script>
		<script src="<?= base_url('vendor/lazysizes/lazysizes.min.js') ?>"></script>
		<script src="<?= base_url('vendor/isotope/jquery.isotope.min.js') ?>"></script>
		<script src="<?= base_url('vendor/owl.carousel/owl.carousel.min.js') ?>"></script>
		<script src="<?= base_url('vendor/magnific-popup/jquery.magnific-popup.min.js') ?>"></script>
		<script src="<?= base_url('vendor/vide/jquery.vide.min.js') ?>"></script>
		<script src="<?= base_url('vendor/vivus/vivus.min.js') ?>"></script>
		<script src="<?= base_url('vendor/bootstrap-star-rating/js/star-rating.min.js') ?>"></script>
		<script src="<?= base_url('vendor/bootstrap-star-rating/themes/krajee-fas/theme.min.js') ?>"></script>
		<script src="<?= base_url('vendor/jquery.countdown/jquery.countdown.min.js') ?>"></script>
		<script src="<?= base_url('js/theme.js') ?>"></script>

		<script src="<?= base_url('js/views/view.shop.js') ?>"></script>
		<script src="<?= base_url('js/theme.init.js') ?>"></script>

		<!-- New JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.12.0/af-2.4.0/b-2.2.3/b-colvis-2.2.3/b-html5-2.2.3/b-print-2.2.3/cr-1.5.6/date-1.1.2/fc-4.1.0/fh-3.2.3/kt-2.7.0/r-2.3.0/rg-1.2.0/rr-1.2.8/sc-2.0.6/sb-1.3.3/sp-2.0.1/sl-1.4.0/sr-1.1.1/datatables.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment.min.js"></script>

        <!-- Script -->
		<?= $this->include('layouts/script') ?>

        <?= $this->renderSection('script') ?>

   	</body>
   
</html>