<?= $this->extend('layouts/layouts') ?>
 
<?= $this->section('title') ?>Status Peminjaman<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="overflow-hidden mb-1">
		<h2 class="font-weight-normal text-7 mb-0"><strong class="font-weight-extra-bold">Status Peminjaman</strong></h2>
	</div>
	<div class="overflow-hidden mb-4 pb-3">
		<p class="mb-0">Daftar buku yang ingin anda pinjam!</p>
	</div>
    <table class="table table-striped" id="dataTable"></table>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script>
        var table;

        function generateTable() {
            table = $('#dataTable').DataTable({
                "destroy": true,
                "ajax": {
                    "url": "<?= site_url('student/api/waiting-list') ?>",
                    "type": "GET"
                },
                "columnDefs": [
                    { "className": "text-right", "targets": [1] },
                ],
                "columns": [
                    {
                        title: "Nama Buku",
                        data: "name"
                    },
                    {
                        title: "Durasi Pinjam (Hari)",
                        data: "day_loan"
                    },
                    {
                        title: "Tanggal Permintaan",
                        data: "date_created",
                        render: function (data, type, row ) {
                            return moment(data).format('DD MMMM YYYY')
                        }
                    },
                    {
                        title: "Status",
                        render: function (data, type, row) {
                            if (row.status == 0) return `<strong class="text-warning">Pending</strong>`
                            else if (row.status == 1) return `<strong class="text-primary">Menunggu Dikirim</strong>`
                            else if (row.status == 2) return `<strong class="text-danger">Rejected</strong>`

                            return `<strong class="text-success">Terkirim ke Email</strong>`
                        }
                    }
                ]
            })
        }

        $(document).ready(function() {
            generateTable()
        })
	</script>
<?= $this->endSection() ?>