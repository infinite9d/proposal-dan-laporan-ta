<?= $this->extend('layouts/layouts') ?>
 
<?= $this->section('title') ?>Status Ebook<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="overflow-hidden mb-1">
		<h2 class="font-weight-normal text-7 mb-0"><strong class="font-weight-extra-bold">Status Ebook</strong></h2>
	</div>
	<div class="overflow-hidden mb-4 pb-3">
		<p class="mb-0">Mengecek status buku TA yang anda tambah!</p>
	</div>
    <table class="table table-striped" id="dataTable"></table>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script>
        var table;

        function generateTable() {
            table = $('#dataTable').DataTable({
                "destroy": true,
                "ajax": {
                    "url": "<?= site_url('student/api/select-by-user') ?>",
                    "type": "GET"
                },
                "columns": [
                    {
                        title: "Judul TA",
                        data: "name"
                    },
                    {
                        title: "Poster TA",
                        data: "image",
                        render: function (data, type, row) {
                            return `<img src="${data}" width="200">`
                        }
                    },
                    {
                        title: "File",
                        data: "file",
                        render: function (data, type, row) {
                            return `<a href="${data}" target="_blank">Link</a>`
                        }
                    },
                    {
                        title: "Tanggal Upload",
                        data: "dt_created",
                        render: function (data, type, row) {
                            return moment(data).format('DD MMM YYYY HH:mm:ss')
                        }
                    },
                    {
                        title: "Status",
                        render: function (data, type, row) {
                            if (row.status == 0) return `<strong class="text-warning">Pending</strong>`
                            else if (row.status == 1) return `<strong class="text-success">Approved</strong>`

                            return `<strong class="text-danger">Rejected</strong>`
                        }
                    }
                ]
            })
        }

        $(document).ready(function() {
            generateTable()
        })
	</script>
<?= $this->endSection() ?>