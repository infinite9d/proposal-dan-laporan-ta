<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');

$routes->post('register', 'AuthController::register');
$routes->post('login', 'AuthController::login');
$routes->get('logout', 'AuthController::logout');

$routes->post('search', 'Home::search');

$routes->group('admin', ['filter' => 'authAdmin'], function($routes)
{
    $routes->get('data-student', 'Home::data_student');
    $routes->get('borrow-confirmation', 'Home::borrow_confirmation');
    $routes->get('request-revoke', 'Home::request_revoke');
    $routes->get('check-ebook', 'Home::check_ebook');
    $routes->get('add-ebook', 'Home::add_ebook_admin');

    $routes->group('book', function($routes)
    {
        $routes->post('insert', 'BookController::insert');
        $routes->post('update', 'BookController::update');
        $routes->post('delete', 'BookController::delete');
    });

    $routes->group('request', function($routes)
    {
        $routes->post('approve', 'RequestController::approve');
        $routes->post('reject', 'RequestController::reject');
        $routes->post('revoke', 'RequestController::revoke');
        $routes->post('confirm-ebook', 'RequestController::confirm_ebook');
        $routes->post('reject-ebook', 'RequestController::reject_ebook');
    });

    $routes->group('api', function($routes)
    {
        $routes->get('select-user', 'ApiController::select_user');
        $routes->get('waiting-confirmation', 'ApiController::waiting_confirmation');
        $routes->get('select-revoke', 'ApiController::select_revoke');
        $routes->get('select-check', 'ApiController::select_check');
    });
});

$routes->group('student', ['filter' => 'authStudent'], function($routes)
{
    $routes->get('borrowed-status', 'Home::borrowed_status');
    $routes->get('ebook-status', 'Home::ebook_status_mhs');
    $routes->get('add-ebook', 'Home::add_ebook_student');

    $routes->group('book', function($routes)
    {
        $routes->post('insert', 'BookController::insert');
        $routes->post('request', 'RequestController::request_ebook');
    });

    $routes->group('api', function($routes)
    {
        $routes->get('waiting-list', 'ApiController::waiting_list');
        $routes->get('select-by-user', 'ApiController::select_by_user');
    });
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
