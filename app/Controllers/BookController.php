<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;

class BookController extends BaseController
{
    public function insert()
    {
        if ($this->request->isAJAX()) {
            $cFile = '';
            $student        = $this->request->getPost('student');
            $name           = $this->request->getPost('name');
            $image          = $this->request->getPost('image');
            $description    = $this->request->getPost('description');
            $is_active      = $this->request->getPost('is_active');

            // Validation
			$validation =  \Config\Services::validation();
			$validation->withRequest($this->request);

			$validation->setRules([
				'student'	=> [
					'label' => 'Student',
					'rules' => 'required|numeric',
					'errors' => [
						'required' => 'Book must have {field} provided'
					]
				],
				'name'	=> [
					'label' => 'Name',
					'rules' => 'required',
					'errors' => [
						'required' => 'Book must have {field} provided'
					]
                ],
                'image'	=> [
					'label' => 'Poster',
					'rules' => 'required',
					'errors' => [
						'required' => 'Book must have {field} provided'
					]
				]
			]);

            if (! $validation->run()) {
				echo json_encode(array(
					'code'		=> 400,
					'success'	=> false,
					'message'	=> "Data is'nt valid",
					'data'		=> [
						'error_name' => $validation->getError('name'),
						'error_image' => $validation->getError('image')
					]
				));
			}
			else {
                if ($_FILES['file']['size'] != 0) {
                    if ($_FILES["file"]["type"] == "application/pdf") {
                        if (function_exists('curl_file_create'))
                            $cFile = curl_file_create($_FILES["file"]["tmp_name"], $_FILES["file"]["type"], $_FILES["file"]["name"]);
                        else
                            $cFile = '@' . realpath($_FILES["file"]["tmp_name"], $_FILES["file"]["type"], $_FILES["file"]["name"]);

                        $parameter = array(
                            'student'       => $student,
                            'name'          => $name,
                            'image'         => $image,
                            'description'   => $description,
                            'file'          => $cFile,
                            'is_active'     => $is_active
                        );
                        $response = curl_file_upload(API_URL . 'books/insert', $parameter);
            
                        echo json_encode($response);
                    }
                    else {
                        echo json_encode(array(
                            'code'      => 400,
                            'success'   => false,
                            'data'      => [
                                'error_file' => 'Book must PDF extension'
                            ],
                            'message'   => 'File extension must .pdf'
                        ));
                    }
                }
                else {
                    echo json_encode(array(
                        'code'      => 400,
                        'success'   => false,
                        'data'      => [
                            'error_file' => 'Book must have file (PDF) provided'
                        ],
                        'message'   => 'File is required'
                    ));
                }
            }
        }
    }

}
