<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class RequestController extends BaseController
{
	// HOME
	public function request_ebook()
	{
		if ($this->request->isAJAX()) {
			$ebook = $this->request->getPost('ebook');
			$days = $this->request->getPost('days');

			if ($_SESSION['user']['public_key'] != '') {
				$url = API_URL . 'books/request';
				$parameter = array(
					'student' 	=> $_SESSION['user']['code'],
					'ebook' 	=> $ebook,
					'days' 		=> $days
				);
				$response = curl($url, $parameter);

				echo json_encode($response);
			}
			else {
				echo json_encode(array(
                    'code' => 400,
                    'success' => false,
                    'data' => [],
                    'message' => 'Public Key Mahasiswa belum ada'
                ));
			}
		}
	}

	// PEMINJAMAN EBOOK
	public function approve()
	{
		if ($this->request->isAJAX()) {
			$code = $this->request->getPost('code');

			if ($_SESSION['user']['public_key'] != '') {
				$url = API_URL . 'loan/approve';
				$parameter = array(
					'code' => $code
				);
				$response = curl($url, $parameter);

				echo json_encode($response);
			}
			else {
				echo json_encode(array(
                    'code' => 400,
                    'success' => false,
                    'data' => [],
                    'message' => 'Public Key Admin belum ada'
                ));
			}
		}
	}

	public function reject()
	{
		if ($this->request->isAJAX()) {
			$code = $this->request->getPost('code');
			$reason = $this->request->getPost('reason');

			if ($_SESSION['user']['public_key'] != '') {
				$url = API_URL . 'loan/reject';
				$parameter = array(
					'code'		=> $code,
					'reason'	=> $reason
				);
				$response = curl($url, $parameter);

				echo json_encode($response);
			}
			else {
				echo json_encode(array(
                    'code' => 400,
                    'success' => false,
                    'data' => [],
                    'message' => 'Public Key Admin belum ada'
                ));
			}
		}
	}

	// REQUEST REVOKE
	public function revoke()
	{
		if ($this->request->isAJAX()) {
			$code = $this->request->getPost('code');
			$nrp = $this->request->getPost('nrp');

			$url = API_URL . 'revoke/update';
			$parameter = array(
				'code'	=> $code,
				'nrp' 	=> $nrp
			);
			$response = curl($url, $parameter);

			echo json_encode($response);
		}
	}

	// CEK EBOOK
	public function confirm_ebook()
	{
		if ($this->request->isAJAX()) {
			$code = $this->request->getPost('code');
			$emailDest = $this->request->getPost('email');

			// Send Mail
			$email = \Config\Services::email();

			$email->setFrom('suryanatarobby@gmail.com', 'Admin E-Lib');
			$email->setTo($emailDest);

			$email->setSubject('(Notifikasi) Penerimaan Upload Ebook Perpustakaan TA ISTTS');
			$email->setMessage('Hai ! buku atau ebook yang anda upload telah disetujui oleh admin. Anda bisa cek buku yang telah diupload pada website perpustakaan TA ISTTS.');

			if (! $email->send()) {
				echo json_encode(array(
					'code'		=> 400,
					'success'	=> false,
					'message'	=> "Email approve can't send to Mahasiswa",
					'data'		=> []
				));
			}
			else {
				$url = API_URL . 'books/confirmation';
				$parameter = array(
					'code' => $code
				);
				$response = curl($url, $parameter);

				echo json_encode($response);
			}
		}
	}

	public function reject_ebook()
	{
		if ($this->request->isAJAX()) {
			$code = $this->request->getPost('code');
			$emailDest = $this->request->getPost('email');
			$nrp = $this->request->getPost('nrp');

			// Send Mail
			$email = \Config\Services::email();

			$email->setFrom('suryanatarobby@gmail.com', 'Admin E-Lib');
			$email->setTo($emailDest);

			$email->setSubject('(Notifikasi) Penolakan Upload Ebook Perpustakaan TA ISTTS');
			$email->setMessage('Hai ! maaf buku atau ebook yang anda upload telah ditolak oleh admin. Harap cek kembali buku yang telah diupload pada menu Cek Ebook dan lakukan upload lagi.');

			if (! $email->send()) {
				echo json_encode(array(
					'code'		=> 400,
					'success'	=> false,
					'message'	=> "Email reject can't send to Mahasiswa",
					'data'		=> []
				));
			}
			else {
				$url = API_URL . 'books/reject';
				$parameter = array(
					'code'	=> $code,
					'nrp'	=> $nrp
				);
				$response = curl($url, $parameter);
	
				echo json_encode($response);
			}
		}
	}
}
