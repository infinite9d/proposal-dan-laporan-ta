<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class ApiController extends BaseController
{
	// ADMIN
	public function select_user()
	{
		if ($this->request->isAJAX()) {
			$url = API_URL . 'user/select';
			$parameter = array();
			$response = curl($url, $parameter);

			echo json_encode($response);
		}
	}

	public function waiting_confirmation()
	{
		if ($this->request->isAJAX()) {
			$url = API_URL . 'waiting/waiting-confirmation';
			$parameter = array();
			$response = curl($url, $parameter);

			echo json_encode($response);
		}
	}

	public function select_revoke()
	{
		if ($this->request->isAJAX()) {
			$url = API_URL . 'revoke/select';
			$parameter = array();
			$response = curl($url, $parameter);

			echo json_encode($response);
		}
	}

	public function select_check()
	{
		if ($this->request->isAJAX()) {
			$url = API_URL . 'books/select-check';
			$parameter = array();
			$response = curl($url, $parameter);

			echo json_encode($response);
		}
	}

	// MAHASISWA
	public function waiting_list()
	{
		if ($this->request->isAJAX()) {
			$url = API_URL . 'waiting/waiting-list';
			$parameter = array(
				'code' => $_SESSION['user']['code']
			);
			$response = curl($url, $parameter);

			echo json_encode($response);
		}
	}

	public function select_by_user()
	{
		if ($this->request->isAJAX()) {
			$url = API_URL . 'books/select-by-user';
			$parameter = array(
				'code' => $_SESSION['user']['code']
			);
			$response = curl($url, $parameter);

			echo json_encode($response);
		}
	}
}
