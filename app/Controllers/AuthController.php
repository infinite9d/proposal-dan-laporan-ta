<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class AuthController extends BaseController
{
	public function register()
	{
		if ($this->request->isAJAX()) {
			$faculty 	= $this->request->getPost('faculty');
			$username 	= $this->request->getPost('username');
			$password 	= $this->request->getPost('password');
			$name 		= $this->request->getPost('name');
			$emailDest 	= $this->request->getPost('email');

			// Validation
			$validation =  \Config\Services::validation();
			$validation->withRequest($this->request);

			$validation->setRules([
				'faculty'	=> [
					'label' => 'Faculty',
					'rules' => 'required|numeric',
					'errors' => [
						'required' => 'All accounts must have {field} provided'
					]
				],
				'username'	=> [
					'label' => 'NRP',
					'rules' => 'required|exact_length[9]|numeric',
					'errors' => [
						'required' => 'All accounts must have {field} provided'
					]
				],
				'password'	=> [
					'label' => 'Password',
					'rules' => 'required|min_length[8]',
					'errors' => [
						'required'		=> 'All accounts must have {field} provided',
						'min_length'	=> 'Your password is too short. Minimum 10 characters'
					]
				],
				'name'	=> [
					'label' => 'Name',
					'rules' => 'required|alpha_space|max_length[50]',
					'errors' => [
						'required' => 'All accounts must have {field} provided'
					]
				],
				'email' => [
					'label' => 'Email',
					'rules' => 'required|valid_email|not_in_list[mhs.stts.edu]|max_length[50]',
					'errors' => [
						'required' 		=> 'All accounts must have {field} provided',
						'not_in_list' 	=> 'Must use iSTTS email'
					]
				]
			]);

			if (! $validation->run()) {
				echo json_encode(array(
					'code'		=> 400,
					'success'	=> false,
					'message'	=> "Data is'nt valid",
					'data'		=> [
						'error_username' => $validation->getError('username'),
						'error_password' => $validation->getError('password'),
						'error_name' => $validation->getError('name'),
						'error_email' => $validation->getError('email')
					]
				));
			}
			else {
				$emailDecode = rawurldecode($emailDest);
				$urlVerification = API_URL . 'confirm-email?email=' . $emailDecode;

				// Send Mail
				$email = \Config\Services::email();

				$email->setFrom('suryanatarobby@gmail.com', 'Admin E-Lib');
				$email->setTo($emailDest);

				$email->setSubject('Verifikasi Email Perpustakaan TA ISTTS');
				$email->setMessage('Hai ! lakukan konfirmasi email pada tombol ini <a href="' . $urlVerification . '" target="_blank"><button type="button">Konfirmasi Email</button></a>');

				if (! $email->send()) {
					echo json_encode(array(
						'code'		=> 400,
						'success'	=> false,
						'message'	=> "Email verification can't send to Mahasiswa",
						'data'		=> []
					));
				}
				else {
					$url = API_URL . 'auth/register';
					$parameter = array(
						'faculty' 	=> $faculty,
						'username' 	=> $username,
						'password' 	=> $password,
						'name' 		=> $name,
						'email' 	=> $emailDest
					);
					$response = curl($url, $parameter);

					echo json_encode($response);
				}
			}
		}
	}

	public function login()
	{
		if ($this->request->isAJAX()) {
			$username = $this->request->getPost('username');
			$password = $this->request->getPost('password');

			$url = API_URL . 'auth/login';
			$parameter = array(
				'username' => $username,
				'password' => $password
			);
			$response = curl($url, $parameter);

			if ($response['success']) {
				$dataLogin = [
					'code'			=> $response['data']['code'],
					'username'		=> $response['data']['username'],
					'name'  		=> $response['data']['name'],
					'public_key'	=> $response['data']['public_key'],
					'status'     	=> $response['data']['status']
				];
				
				$this->session->set('user', $dataLogin);
			}

			echo json_encode($response);
		}
	}

	public function logout()
	{
		unset($_SESSION['user']);
		return redirect()->route('/');
	}
}
