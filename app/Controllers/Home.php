<?php

namespace App\Controllers;

// use App\Models\Mahasiswa;

class Home extends BaseController
{
	public function __construct() {
        // $this->Mahasiswa = new Mahasiswa;
    }

	public function search() {
		if ($this->request->isAJAX()) {
			$name = $this->request->getPost('name');
			$faculty = $this->request->getPost('faculty');

			$url = API_URL . 'books/select';
			$parameter = array(
				'name' 		=> $name,
				'faculty' 	=> $faculty
			);

			$response = curl($url, $parameter);
			$this->session->set('books', $response['data']);

			echo json_encode($response);
		}
	}

	public function index()
	{
		if (isset($_SESSION['books'])) {
			$data['books'] = $_SESSION['books'];
		}
		else {
			$url = API_URL . 'books/select';
			$parameter = array(
				'name' => "",
				'faculty' => "%"
			);
	
			$response = curl($url, $parameter);
			$data['books'] = $response['data'];
		}

		return view('index', $data);
	}

	// ADMIN
	public function data_student()
	{
		return view('admin/data_student');
	}

	public function borrow_confirmation()
	{
		return view('admin/borrow_confirmation');
	}

	public function request_revoke()
	{
		return view('admin/request_revoke');
	}

	public function check_ebook()
	{
		return view('admin/check_ebook');
	}

	public function add_ebook_admin()
	{
		$url = API_URL . 'user/select';
		$parameter = array();
		$response = curl($url, $parameter);
		$data['users'] = $response['data'];

		return view('admin/add_ebook', $data);
	}

	// STUDENT
	public function borrowed_status()
	{
		return view('student/borrowed_status');
	}

	public function ebook_status_mhs()
	{
		return view('student/ebook_status');
	}

	public function add_ebook_student()
	{
		$data['code'] = $_SESSION['user']['code'];
		return view('student/add_ebook', $data);
	}
}
