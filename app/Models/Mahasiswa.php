<?php

namespace App\Models;

use CodeIgniter\Model;

class Mahasiswa extends Model
{
    protected $table      = 'users';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['name', 'email'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    /**
     * Called during initialization. Appends
     * our custom field to the module's model.
     */
    protected function initialize()
    {
        $this->allowedFields[] = 'middlename';
    }

    public function get() {
        $user = $this->find($user_id);
        // $users = $userModel->where('active', 1)->findAll();
    }

    public function insert() {
        $data = [
            'username' => 'darth',
            'email'    => 'd.vader@theempire.com'
        ];
        
        $this->insert($data);
    }
    public function update() {
        $data = [
            'username' => 'darth',
            'email'    => 'd.vader@theempire.com'
        ];
        
        $this->update($id, $data);
    }
    public function delete() {
        $data = [
            'username' => 'darth',
            'email'    => 'd.vader@theempire.com'
        ];
        
        $this->update($id, $data);
    }
}